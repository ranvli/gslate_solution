﻿using DrinksMachineInterface.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrinksMachineInterface
{
    public partial class Admin : Form
    {
        public Admin()
        {
            InitializeComponent();
        }

        private async void Admin_Load(object sender, EventArgs e)
        {
            var apiDrinksMachine = ApiCall.InstanceAPIDrinksMachine();

            var availableCoins = await apiDrinksMachine.CoinGetAllAsync();
            var availableProducts = await apiDrinksMachine.ProductGetAllAsync();

            foreach (var product in availableProducts.Data)
            {
                lbDrinks.Items.Add($"Product name: {product.ProductName} - Qty Available: {product.QtyAvailable}");
            }
        }
    }
}
