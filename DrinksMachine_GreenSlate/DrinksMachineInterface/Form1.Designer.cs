﻿
namespace DrinksMachineInterface
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lvwImages = new System.Windows.Forms.ListView();
            this.imlLargeIcons = new System.Windows.Forms.ImageList(this.components);
            this.imlSmallIcons = new System.Windows.Forms.ImageList(this.components);
            this.btnAdmin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvwImages
            // 
            this.lvwImages.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvwImages.FullRowSelect = true;
            this.lvwImages.GridLines = true;
            this.lvwImages.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvwImages.HideSelection = false;
            this.lvwImages.Location = new System.Drawing.Point(12, 74);
            this.lvwImages.Name = "lvwImages";
            this.lvwImages.Size = new System.Drawing.Size(776, 302);
            this.lvwImages.TabIndex = 4;
            this.lvwImages.TileSize = new System.Drawing.Size(128, 128);
            this.lvwImages.UseCompatibleStateImageBehavior = false;
            // 
            // imlLargeIcons
            // 
            this.imlLargeIcons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imlLargeIcons.ImageSize = new System.Drawing.Size(128, 128);
            this.imlLargeIcons.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // imlSmallIcons
            // 
            this.imlSmallIcons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imlSmallIcons.ImageSize = new System.Drawing.Size(32, 32);
            this.imlSmallIcons.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // btnAdmin
            // 
            this.btnAdmin.Location = new System.Drawing.Point(12, 407);
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.Size = new System.Drawing.Size(94, 31);
            this.btnAdmin.TabIndex = 5;
            this.btnAdmin.Text = "Admin";
            this.btnAdmin.UseVisualStyleBackColor = true;
            this.btnAdmin.Click += new System.EventHandler(this.btnAdmin_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnAdmin);
            this.Controls.Add(this.lvwImages);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ListView lvwImages;
        public System.Windows.Forms.ImageList imlLargeIcons;
        public System.Windows.Forms.ImageList imlSmallIcons;
        private System.Windows.Forms.Button btnAdmin;
    }
}

