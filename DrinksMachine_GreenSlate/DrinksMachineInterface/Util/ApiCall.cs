﻿
namespace DrinksMachineInterface.Util
{
    public class ApiCall
    {
        public static DrinksMachineAPI.Client InstanceAPIDrinksMachine()
        {
            //Instanciar servicio y retornarlo
            var service = new DrinksMachineAPI.Client("http://localhost:65378",
                UtilServices.GetClient("na", "na"));
            return service;
        }
    }
}
