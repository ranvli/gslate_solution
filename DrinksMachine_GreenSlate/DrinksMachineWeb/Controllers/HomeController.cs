﻿using DrinksMachineAPI;
using DrinksMachineWeb.Models;
using DrinksMachineWeb.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Process(ProcessDto process)
        {
            //get values from machine
            double totalInserted = process.totalInserted;
            double totalPurchased = process.totalPurchase;
            string products = process.productsHI;

            var drinksMachineDto = new DrinksMachineDto();

            if (totalInserted >= totalPurchased)
            {
                //instantiace api
                var apiDrinksMachine = ApiCall.InstanceAPIDrinksMachine();

                var productsToSale = new List<ProductToSaleDto>();

                //Convert products array to list and order products by id
                var productsList = products.Split(',').ToList();
                productsList = productsList.OrderBy(x => x).Select(s => s.Replace("buy_", "")).ToList();

                //load products to sale DTO
                string lastpId = "";
                foreach (string productId in productsList)
                {
                    if (productId != lastpId)
                    {
                        var productToSale = new ProductToSaleDto();
                        productToSale.ProductId = int.Parse(productId);
                        productToSale.Qty = productsList.Count(x => x == productId);
                        productsToSale.Add(productToSale);
                        lastpId = productId;
                    }
                }

                //create body dto
                IncomingProductSaleDto incomingProductSaleDto = new IncomingProductSaleDto();
                incomingProductSaleDto.BillCoinCounterTotal = totalInserted;
                incomingProductSaleDto.ProductsToSale = productsToSale;

                //call request api
                var response = (await apiDrinksMachine.ProductSaleAsync(incomingProductSaleDto)).Data;
                drinksMachineDto.Coins = response.NewAvailableCoins.ToList();
                drinksMachineDto.Products = response.NewAvailableProducts.ToList();
                drinksMachineDto.CoinsToReturn = response.CoinsToReturn.ToList();
                drinksMachineDto.DrinksToReturn = response.DrinksToReturn.ToList();
            }
            else
            {
                drinksMachineDto.Message = "Not enough credit";
            }

            return View("Index", drinksMachineDto);
        }
        public async Task<IActionResult> Index()
        {
            var apiDrinksMachine = ApiCall.InstanceAPIDrinksMachine();

            var availableCoins = (await apiDrinksMachine.CoinGetAllAsync()).Data;
            var availableProducts = (await apiDrinksMachine.ProductGetAllAsync()).Data;

            //main model
            DrinksMachineDto drinksMachineDto = new DrinksMachineDto();
            drinksMachineDto.Products = availableProducts.ToList();
            drinksMachineDto.Coins = availableCoins.ToList();
            drinksMachineDto.CoinsToReturn = new List<CoinDto>();
            drinksMachineDto.DrinksToReturn = new List<string>();

            return View(drinksMachineDto);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
