﻿using DrinksMachineAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineWeb.Models
{
    public class DrinksMachineDto
    {
        public List<ProductDto> Products { get; set; }
        public List<CoinDto> Coins { get; set; }
        public string Message { get; set; }
        public List<CoinDto> CoinsToReturn { get; internal set; }
        public List<string> DrinksToReturn { get; internal set; }
    }
}
