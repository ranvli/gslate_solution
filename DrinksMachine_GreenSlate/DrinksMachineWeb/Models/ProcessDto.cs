﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineWeb.Models
{
    public class ProcessDto
    {
        public double totalPurchase { get; set; }
        public double totalInserted { get; set; }
        public string productsHI { get; set; }
    }
}
