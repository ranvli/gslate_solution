﻿
namespace DrinksMachineWeb.Util
{
    public class ApiCall
    {
        public static DrinksMachineAPI.swagger1Client InstanceAPIDrinksMachine()
        {
            //Instanciar servicio y retornarlo
            var service = new DrinksMachineAPI.swagger1Client("http://localhost:65378",
                UtilServices.GetClient("na", "na"));
            return service;
        }
    }
}
