﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace DrinksMachineWeb.Util
{
    public class UtilServices
    {
        public static HttpClient GetClient(string token, string secret)
        {
            HttpClient httpClient = null;

            //Instanciar cliente
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            httpClient = new HttpClient(clientHandler);

            httpClient.DefaultRequestHeaders.Add("X-ApiKey", token);
            httpClient.DefaultRequestHeaders.Add("X-SecretKey", secret);

            return httpClient;
        }
    }

    internal class Messages
    {
        public string InternalCode { get; set; }
        public string Message { get; set; }
    }
}
