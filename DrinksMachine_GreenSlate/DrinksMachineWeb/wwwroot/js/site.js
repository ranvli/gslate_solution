﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var totalInserted = 0;
var totalPurchased = 0;
var productsId = [];

$("#insert01").on("click", function (event) {
    event.preventDefault();
    totalInserted += 0.01;
    UpdateTotal();
});


$("#insert05").on("click", function (event) {
    event.preventDefault();
    totalInserted += 0.05;
    UpdateTotal();
});

$("#insert10").on("click", function (event) {
    event.preventDefault();
    totalInserted += 0.10;
    UpdateTotal();
});

$("#insert25").on("click", function (event) {
    event.preventDefault();
    totalInserted += 0.25;
    UpdateTotal();
});

function UpdateTotal()
{
    $("#totalInserted").val(totalInserted);
    $("#totalPurchase").val(totalPurchased);
    $("#productsHI").val(productsId); //store array

    if (totalInserted < totalPurchased) {
        $("#btnPurchase").prop('disabled', true);
        $("#btnPurchase").prop('style', "background:red;");
    }
    else
    {
        $("#btnPurchase").prop('disabled', false);
        $("#btnPurchase").prop('style', "background:green;");
    }
}

var btns = document.getElementsByClassName("btnBuy");
for (i = 0; i < btns.length; i++)
{
    $("#" + btns[i].id).on("click", function (event) {
        event.preventDefault();
        totalPurchased += parseFloat($(this).val());
        productsId.push($(this).attr('id'));
        UpdateTotal();
    });
}