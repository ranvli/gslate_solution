﻿using AutoMapper;
using DrinksMachineAPI.DrinksMachineAPI.Dto;
using DrinksMachineAPI.DrinksMachineAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineAPI
{
    class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Product, ProductDto>();
            CreateMap<Coin, CoinDto>();
        }
    }
}
