﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using DrinksMachineAPI.DrinksMachineAPI.Dto;
using DrinksMachineAPI.DrinksMachineAPI.Dto.Base;
using DrinksMachineAPI.DrinksMachineAPI.Models;
using DrinksMachineAPI.DrinksMachineAPI.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;


namespace DrinksMachineAPI.DrinksMachineAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoinController : ControllerBase
    {
        private ICoinService _coinService;
        private readonly IMapper _mapper;

        public CoinController(ICoinService coinService, IMapper mapper)
        {
            _coinService = coinService;
            _mapper = mapper;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("coin-get-all")]
        [SwaggerResponse((int)HttpStatusCode.OK, "If coins were found", typeof(ResponseDto<IEnumerable<CoinDto>>))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Returns for any uncontroller exception")]
        public IActionResult CoinGetAll()
        {
            ResponseDto<IEnumerable<CoinDto>> response = new ResponseDto<IEnumerable<CoinDto>>();

            try
            {
                var allCoins = _coinService.GetAll();
                response.Data = _mapper.Map<IEnumerable<CoinDto>>(allCoins);
                return Ok(response);
            }
            catch (Exception e)
            {
                response.Error.ExceptionMessage = e.Message;
                response.Error.Message = "Internal Error";
                response.Error.Code = 1;
                return BadRequest(response);
                throw;
            }
        }
    }
}