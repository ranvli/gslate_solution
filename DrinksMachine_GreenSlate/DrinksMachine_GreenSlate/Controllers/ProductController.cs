﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using DrinksMachineAPI.DrinksMachineAPI.Dto;
using DrinksMachineAPI.DrinksMachineAPI.Dto.Base;
using DrinksMachineAPI.DrinksMachineAPI.Models;
using DrinksMachineAPI.DrinksMachineAPI.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;


namespace DrinksMachineAPI.DrinksMachineAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductService _productService;
        private ICoinService _coinService;
        private readonly IMapper _mapper;

        public ProductController(IProductService productService, ICoinService coinService, IMapper mapper)
        {
            _productService = productService;
            _coinService = coinService;
            _mapper = mapper;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("product-get-all")]
        [SwaggerResponse((int)HttpStatusCode.OK, "If products were found", typeof(ResponseDto<IEnumerable<ProductDto>>))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Returns for any uncontroller exception")]
        public IActionResult ProductGetAll()
        {
            ResponseDto<IEnumerable<ProductDto>> response = new ResponseDto<IEnumerable<ProductDto>>();

            try
            {
                var allProducts = _productService.GetAll();
                var productsResponse = new List<ProductDto>();
                response.Data = _mapper.Map<List<ProductDto>>(allProducts);
                return Ok(response);
            }
            catch (Exception e)
            {
                response.Error.ExceptionMessage = e.Message;
                response.Error.Message = "Internal Error";
                response.Error.Code = 1;
                return BadRequest(response);
                throw;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("product-sale")]
        [SwaggerResponse((int)HttpStatusCode.OK, "If product were purchased", typeof(ResponseDto<ResultProductSaleDto>))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Returns for any uncontroller exception")]
        public IActionResult ProductSale([FromBody] IncomingProductSaleDto newSaleDto)
        {
            ResponseDto<ResultProductSaleDto> response = new ResponseDto<ResultProductSaleDto>();

            if (ModelState.IsValid)
            {
                try
                {
                    ResultProductSaleDto resultProductSaleDto = new ResultProductSaleDto();
                    resultProductSaleDto.DrinksToReturn = new List<string>();

                    bool notEnoughCoins = false;
                    var coinsInv = _coinService.GetAll();
                    if (coinsInv.Any(c => c.QtyAvailable == 0))
                    {
                        notEnoughCoins = true;
                    }

                    if (!notEnoughCoins)
                    {
                        //Calculate totals
                        double total = 0;
                        foreach (var productToSaleIdQty in newSaleDto.ProductsToSale)
                        {
                            var productToSale = _productService.Get(productToSaleIdQty.ProductId);
                            //point 3 (Validate available inventory)
                            if (productToSale.QtyAvailable >= productToSaleIdQty.Qty)
                            {
                                total += productToSale.Price * productToSaleIdQty.Qty;
                                for (int i = 0; i < productToSaleIdQty.Qty; i++)
                                {
                                    resultProductSaleDto.DrinksToReturn.Add($"Dispense {productToSale.ProductName}"); //point 2 (Return command to frontend to dispense)
                                }
                            }
                            else
                            {
                                resultProductSaleDto.DrinksToReturn.Add($"Cannot dispense {productToSale.ProductName} because not enough inventory"); //point 2 (Return command to frontend to dispense)
                            }
                        }

                        double change = Math.Round(newSaleDto.BillCoinCounterTotal, 2) - Math.Round(total, 2);
                        change = Math.Round(change, 2);
                        //Point 1 (check paid amount is more than total)
                        if (change < 0) throw new Exception($"Paid amount is less than total. Paid={newSaleDto.BillCoinCounterTotal}, total={total}");

                        //point 4 (update inventory)
                        foreach (var productToSaleIdQty in newSaleDto.ProductsToSale)
                        {
                            var productToSale = _productService.Get(productToSaleIdQty.ProductId);
                            if (productToSale.QtyAvailable >= productToSaleIdQty.Qty)
                            {
                                productToSale.QtyAvailable -= productToSaleIdQty.Qty;
                                _productService.Update(productToSale);
                            }
                        }

                        //Point 8 calculate change
                        //Calculate coins to return
                        IEnumerable<Coin> coins = _coinService.GetAll();
                        List<Coin> coinsList = coins.OrderByDescending(o => o.CoinValue).ToList();
                        List<CoinDto> coinsToReturn = new List<CoinDto>();

                        bool cancelSale = false;
                        foreach (var currentCoin in coinsList)
                        {
                            int coinQty = 0;
                            bool acceptedCoin = false;

                            do
                            {
                                if (currentCoin.CoinValue <= change)
                                {
                                    change -= currentCoin.CoinValue;
                                    change = Math.Round(change, 2);
                                    coinQty++;
                                    acceptedCoin = true;
                                }
                                else
                                {
                                    break;
                                }
                            } while (change > 0);

                            if (acceptedCoin)
                            {
                                coinsToReturn.Add(new CoinDto() { CoinName = currentCoin.CoinName, QtyAvailable = coinQty, CoinId = currentCoin.CoinId });
                                if (currentCoin.QtyAvailable < coinQty)
                                {
                                    cancelSale = true;
                                }
                                else
                                {
                                    currentCoin.QtyAvailable -= coinQty;
                                    _coinService.Update(currentCoin);
                                }
                            }

                            if (change == 0)
                            {
                                break;
                            }
                        }

                        if (cancelSale)
                        {
                            //rollback inventory products
                            foreach (var productToSaleIdQty in newSaleDto.ProductsToSale)
                            {
                                var productToSale = _productService.Get(productToSaleIdQty.ProductId);
                                productToSale.QtyAvailable += productToSaleIdQty.Qty;
                                _productService.Update(productToSale);
                            }

                            //rollback inventory coins
                            foreach (var coinToReturn in coinsToReturn)
                            {
                                var ccoin = _coinService.GetAll().FirstOrDefault(c => c.CoinId == coinToReturn.CoinId);
                                ccoin.QtyAvailable += ccoin.QtyAvailable;
                            }

                            resultProductSaleDto.CoinsToReturn = new List<CoinDto>();
                            resultProductSaleDto.DrinksToReturn.Add($"Cannot dispense because Not sufficient change in the inventory");
                        }

                        resultProductSaleDto.CoinsToReturn = coinsToReturn;
                    }
                    else
                    {
                        resultProductSaleDto.CoinsToReturn = new List<CoinDto>();
                        resultProductSaleDto.DrinksToReturn.Add($"Cannot dispense because Not sufficient change in the inventory");
                    }

                    //send updated inventory
                    resultProductSaleDto.NewAvailableProducts = _mapper.Map<List<ProductDto>>(_productService.GetAll()); 
                    resultProductSaleDto.NewAvailableCoins = _mapper.Map<List<CoinDto>>(_coinService.GetAll());

                    response.Data = resultProductSaleDto;

                    return Ok(response);
                }
                catch (Exception e)
                {
                    response.Error.ExceptionMessage = e.Message;
                    response.Error.Message = "Internal Error";
                    response.Error.Code = 1;
                    return BadRequest(response);
                    throw;
                }
            }
            else
            {
                response.Error.ExceptionMessage = "Model validation error";
                response.Error.Message = "Internal Error";
                response.Error.Code = 2;
                return BadRequest(response);
            }
        }
    }
}