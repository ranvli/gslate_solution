﻿using DrinksMachineAPI.DrinksMachineAPI.DAL;
using DrinksMachineAPI.DrinksMachineAPI.DAL.Context;
using DrinksMachineAPI.DrinksMachineAPI.DAL.Contracts;
using DrinksMachineAPI.DrinksMachineAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 

namespace DrinksMachineAPI.DrinksMachineAPI.DAL
{
    public class CoinRepository : Repository<Coin>, ICoinRepository
    {
        DrinksMachineContext drinksMachineContext = null;
        public CoinRepository(DrinksMachineContext context) : base(context)
        {
            drinksMachineContext = context;
        }

        //additional methods here...
               

    }
}
