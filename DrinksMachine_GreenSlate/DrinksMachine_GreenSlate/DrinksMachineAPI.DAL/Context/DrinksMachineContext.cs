﻿using DrinksMachineAPI.DrinksMachineAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineAPI.DrinksMachineAPI.DAL.Context
{
    public partial class DrinksMachineContext : DbContext
    {
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Coin> Coins { get; set; }

        public DrinksMachineContext(DbContextOptions<DrinksMachineContext> options) : base(options)
        {
            //ensure to create inMemory seed data
            this.Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            //Create Basic seed data
            builder.Entity<Product>().HasData
            (
                new Product
                {
                    ProductId = 1,
                    ProductName = "Coke",
                    Price = 0.25,
                    QtyAvailable = 5
                },
                new Product
                {
                    ProductId = 2,
                    ProductName = "Pepsi",
                    Price = 0.25,
                    QtyAvailable = 15
                },
                new Product
                {
                    ProductId = 3,
                    ProductName = "Soda",
                    Price = 0.15,
                    QtyAvailable = 3
                }
            );

            builder.Entity<Coin>().HasData
            (
                new Coin
                {
                    CoinId = 1,
                    CoinName = "Penny",
                    CoinValue = 0.01,
                    QtyAvailable = 100
                },
                new Coin
                {
                    CoinId = 2,
                    CoinName = "Nickels",
                    CoinValue = 0.05,
                    QtyAvailable = 10
                },
                new Coin
                {
                    CoinId = 3,
                    CoinName = "Dime",
                    CoinValue = 0.1,
                    QtyAvailable = 5
                },
                new Coin
                {
                    CoinId = 4,
                    CoinName = "Quarter",
                    CoinValue = 0.25,
                    QtyAvailable = 25
                }
            );
        }
    }


}
