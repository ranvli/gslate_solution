﻿using DrinksMachineAPI.DrinksMachineAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineAPI.DrinksMachineAPI.DAL.Contracts
{
    public interface ICoinRepository : IRepository<Coin>
    {
        
    }
}
