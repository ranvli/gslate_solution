﻿using DrinksMachineAPI.DrinksMachineAPI.DAL;
using DrinksMachineAPI.DrinksMachineAPI.DAL.Context;
using DrinksMachineAPI.DrinksMachineAPI.DAL.Contracts;
using DrinksMachineAPI.DrinksMachineAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 

namespace DrinksMachineAPI.DrinksMachineAPI.DAL
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        DrinksMachineContext drinksMachineContext = null;
        public ProductRepository(DrinksMachineContext context) : base(context)
        {
            drinksMachineContext = context;
        }

        //additional methods here...
               

    }
}
