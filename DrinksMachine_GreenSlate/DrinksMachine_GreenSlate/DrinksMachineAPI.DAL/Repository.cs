﻿using DrinksMachineAPI.DrinksMachineAPI.DAL.Context;
using DrinksMachineAPI.DrinksMachineAPI.DAL.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DrinksMachineAPI.DrinksMachineAPI.DAL
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected DrinksMachineContext DbContext;
        protected readonly DbSet<TEntity> DbSet;

        public Repository(DrinksMachineContext context)
        {
            DbContext = context;
            DbSet = DbContext.Set<TEntity>();
        }

        public TEntity Get(int id)
        {
            return DbSet.Find(id);
        }
        
        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
        }


        public IEnumerable<TEntity> GetAll()
        {
            IEnumerable<TEntity> result = null;
            result = DbSet.ToList();
            return result;
        }

        public void Delete(int id)
        {
            TEntity existing = DbContext.Find<TEntity>(id);
            if (existing != null) DbSet.Remove(existing);
        }

        public int CommitChanges()
        {
            return DbContext.SaveChanges();
        }

        public Task<int> CommitChangesAsync()
        {
            return DbContext.SaveChangesAsync();
        }
    }
}
