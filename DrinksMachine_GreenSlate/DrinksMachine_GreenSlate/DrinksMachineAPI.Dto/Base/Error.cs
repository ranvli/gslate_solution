﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineAPI.DrinksMachineAPI.Dto.Base
{
    public class Error
    {
        public string Message { get; set; }

        public string ExceptionMessage { get; set; }
        public int Code { get; set; }
    }
}
