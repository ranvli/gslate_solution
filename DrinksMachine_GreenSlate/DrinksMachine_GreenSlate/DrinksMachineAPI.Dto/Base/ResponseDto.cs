﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineAPI.DrinksMachineAPI.Dto.Base
{
    public class ResponseDto<T> where T : class
    {
        public Error Error { get; set; }
        public T Data { get; set; }

        public ResponseDto()
        {
            Error = new Error();
        }
    }
}
