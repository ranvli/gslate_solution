﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineAPI.DrinksMachineAPI.Dto
{
    public class ProductDto
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public double Price { get; set; }
        public int QtyAvailable { get; set; }
    }

    public class ProductToSaleDto
    {
        public int ProductId { get; set; }
        [Range(1, 99)]
        public int Qty { get; set; }
    }

    public class IncomingProductSaleDto
    {
        public List<ProductToSaleDto> ProductsToSale { get; set; }
        [Range(0, 99)]
        public double BillCoinCounterTotal { get; set; }
    }

    public class ResultProductSaleDto
    {
        public List<CoinDto> CoinsToReturn { get; set; }
        public List<string> DrinksToReturn { get; set; }
        public List<ProductDto> NewAvailableProducts { get; set; }
        public List<CoinDto> NewAvailableCoins { get; set; }
    }
}
