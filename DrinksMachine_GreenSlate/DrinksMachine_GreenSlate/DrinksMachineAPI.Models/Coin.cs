﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DrinksMachineAPI.DrinksMachineAPI.Models
{
    public class Coin
    {
        [Key]
        public int CoinId { get; set; }
        public string CoinName { get; set; }
        public double CoinValue { get; set; }
        public int QtyAvailable { get; set; }
    }
}
