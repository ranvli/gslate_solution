﻿using DrinksMachineAPI.DrinksMachineAPI.DAL.Contracts;
using DrinksMachineAPI.DrinksMachineAPI.Models;
using DrinksMachineAPI.DrinksMachineAPI.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 

namespace DrinksMachineAPI.DrinksMachineAPI.Services
{
    public class CoinService : Service<Coin>, ICoinService
    {
        private readonly ICoinRepository Repository;

        public CoinService(ICoinRepository repository) : base(repository)
        {
            Repository = repository;
        }
    }
}
