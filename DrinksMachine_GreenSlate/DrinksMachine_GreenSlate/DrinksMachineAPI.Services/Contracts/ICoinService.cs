﻿using DrinksMachineAPI.DrinksMachineAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 

namespace DrinksMachineAPI.DrinksMachineAPI.Services.Contracts
{
    public interface ICoinService : IService<Coin>
    {
        //Add addictional methods to CRUD here...
    }
}
