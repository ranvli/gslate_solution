﻿using DrinksMachineAPI.DrinksMachineAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 

namespace DrinksMachineAPI.DrinksMachineAPI.Services.Contracts
{
    public interface IProductService : IService<Product>
    {
        //Add addictional methods to CRUD here...
    }
}
