﻿using DrinksMachineAPI.DrinksMachineAPI.DAL.Contracts;
using DrinksMachineAPI.DrinksMachineAPI.Models;
using DrinksMachineAPI.DrinksMachineAPI.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 

namespace DrinksMachineAPI.DrinksMachineAPI.Services
{
    public class ProductService : Service<Product>, IProductService
    {
        private readonly IProductRepository Repository;

        public ProductService(IProductRepository repository) : base(repository)
        {
            Repository = repository;
        }
    }
}
